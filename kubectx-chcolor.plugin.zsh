typeset -g -A kubectx_mapping_theme

function precmd() {
  (( $+commands[kubectl] )) || return

  local current_ctx=$(kubectl config current-context 2> /dev/null)

  [[ -n "$current_ctx" ]] || return

  if [[ -n "${(P)kubectx_mapping_theme[$current_ctx]}" ]] ; then
    return
  fi

  # do this only on iterm
  if [[ "$OSTYPE" == darwin* ]] && [[ -n "$ITERM_SESSION_ID" ]] ; then
    iterm2_profile ${kubectx_mapping_theme[$current_ctx]}
  fi

  if [[ -n "$KONSOLE_VERSION" ]] ; then
    konsoleprofile colors=${kubectx_mapping_theme[$current_ctx]}
  fi
}

