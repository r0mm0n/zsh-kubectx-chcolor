#!/usr/bin/env python3
# https://github.com/jamesdh/dotfiles/blob/8fbe5409c6bb26bce9d50e2328359f5a37edd716/roles/osx/files/iTerm/change_default_profile.py
import iterm2
import sys

async def main(connection):
    app = await iterm2.async_get_app(connection)
    # Query for the list of profiles so we can search by name. This returns a
    # subset of the full profiles so it's fast.
    partialProfiles = await iterm2.PartialProfile.async_query(connection)
    # Iterate over each partial profile
    profile_name = sys.argv[1]
    for partial in partialProfiles:
        if partial.name == profile_name:
            # This is the one we're looking for. Change the current session's
            # profile.
            full = await partial.async_get_full_profile()
            await app.current_terminal_window.current_tab.current_session.async_set_profile(full)
            return

iterm2.run_until_complete(main)
