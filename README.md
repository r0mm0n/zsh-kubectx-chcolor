# zsh-kubectx-chcolor

add `kubectx-chcolor` to plugin array of oh-my-zsh
## Mac OS
also add `iterm2` to plugin array of oh-my-zsh before `kubectx-chcolor`

Define some iterm2 profiles and use the name of it in the definition:

`.zshrc`
```sh

kubectx_mapping_theme[kind-ref]="REF"
kubectx_mapping_theme[kind-kind]="PROD"

```


## Konsole

create new color schemes for Konsole and use the name of it in the `.zshrc` like:

`.zshrc`
```sh
kubectx_mapping_theme[kind-ref]="GreenOnBlack"
kubectx_mapping_theme[kind-kind]="WhiteOnBlack"

```

### pure zsh

TODO
